#About

This project is an interpreter for a subset of C, which I call microC or uC for short.
It ~~was~~ is created to learn more about how programming languages work as a personal project.

#Features

The language does not have an "official" feature list yet, however these are some of the things that I woul like to implement into the language:

* A hybrid typing system
	* Each variable has to be declared and given a type although this type may change during runtime
* Control flow
	* Conditional branching
	* Loops
* Assignment
* User defined functions and types
* Arrays

More will be added to this list and eventually a full specification will be added to the wiki.